package com.dalu.games.gameOfThrones.units;

import com.dalu.games.gameOfThrones.house.House;
import com.dalu.games.gameOfThrones.map.Region;

public abstract class Unit {
	
	protected Region region;
	protected House house;

	public abstract int getStrength();

	public int getSiedgeStrength(){
		return getStrength();
	}
	
	public void move(Region region){
		this.region = region;		
	}
	
	public abstract boolean canMoveTo(Region region);
	
	public abstract String getResourceBoundle();

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public House getHouse() {
		return house;
	}

	public void setHouse(House house) {
		this.house = house;
	}	
}
