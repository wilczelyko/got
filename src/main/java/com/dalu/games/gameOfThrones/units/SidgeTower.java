package com.dalu.games.gameOfThrones.units;

public class SidgeTower extends LandUnit {

	@Override
	public int getStrength() {	
		return 0;
	}

	@Override
	public int getSiedgeStrength() {
		return 4;
	}	
	
	@Override
	public String getResourceBoundle() {
		return "unit.siedgeTower";
	}
}
