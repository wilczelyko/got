package com.dalu.games.gameOfThrones.units;

import com.dalu.games.gameOfThrones.house.Route;
import com.dalu.games.gameOfThrones.map.Region;

public abstract class LandUnit extends Unit {
	@Override
	public boolean canMoveTo(Region region) {
		return region.isLand() && (this.region.isNeighbord(region) || this.house.getRoutes().contains(new Route(this.region,region)));
	}
}
