package com.dalu.games.gameOfThrones.units;

import com.dalu.games.gameOfThrones.map.Region;

public class Ship extends Unit {

	@Override
	public int getStrength() {
		return 1;
	}

	@Override
	public boolean canMoveTo(Region region) {
		return !region.isLand() && this.region.isNeighbord(region);
	}
	
	@Override
	public String getResourceBoundle() {
		return "unit.ship";
	}
}
