package com.dalu.games.gameOfThrones.units;

public class Knight extends LandUnit{

	@Override
	public int getStrength() {		
		return 2;
	}
	
	@Override
	public String getResourceBoundle() {
		return "unit.knight";
	}
}
