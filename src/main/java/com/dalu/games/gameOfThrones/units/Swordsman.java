package com.dalu.games.gameOfThrones.units;


public class Swordsman extends LandUnit {

	@Override
	public int getStrength() {		
		return 1;
	}

	@Override
	public String getResourceBoundle() {
		return "unit.swordsman";
	}
}
