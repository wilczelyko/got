package com.dalu.games.gameOfThrones.units;

public class UnitsPool {
	private int ships;
	private int swordmen;
	private int knights;
	private int siedgeTowers;
	
	public UnitsPool(int ships, int swordsman, int knights, int siedgeTowers) {
		this.ships = ships;
		this.swordmen = swordsman;
		this.knights = knights;
		this.siedgeTowers = siedgeTowers;
		
	}
	public void getShip(){
		if(ships>0){
			ships--;			
		} else {
			throw new IllegalStateException("No more units to grab");
		}
	}
	public void returnShip(){
		ships++;
	}
	
	public void getSwordman(){
		if(swordmen>0){
			swordmen--;
		} else {
			throw new IllegalStateException("No more units to grab");
		}
	}
	public void returnSwordman(){
		swordmen++;
	}
	
	public void getKnight(){
		if(knights>0){
			knights--;
		} else {
			throw new IllegalStateException("No more units to grab");
		}
	}
	public void returnKnight(){
		knights++;
	}
	
	public void getSiedgeTower(){
		if(siedgeTowers>0){
			siedgeTowers--;
		} else {
			throw new IllegalStateException("No more units to grab");
		}
	}
	public void returnSiedgeTower(){
		siedgeTowers++;
	}
	
	public int getShips() {
		return ships;
	}
	public void setShips(int ships) {
		this.ships = ships;
	}
	public int getSwordmen() {
		return swordmen;
	}
	public void setSwordmen(int swordmen) {
		this.swordmen = swordmen;
	}
	public int getKnights() {
		return knights;
	}
	public void setKnights(int knights) {
		this.knights = knights;
	}
	public int getSiedgeTowers() {
		return siedgeTowers;
	}
	public void setSiedgeTowers(int siedgeTorers) {
		this.siedgeTowers = siedgeTorers;
	}
	
	

}
