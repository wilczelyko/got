package com.dalu.games.gameOfThrones.house;

import com.dalu.games.gameOfThrones.map.Region;

public class Route {
	private Region first;
	private Region second;

	public Route(Region first, Region second) {
		this.first = first;
		this.second = second;
	}
	public Region getFirst() {
		return first;
	}
	public void setFirst(Region first) {
		this.first = first;
	}
	public Region getSecond() {
		return second;
	}
	public void setSecond(Region second) {
		this.second = second;
	}
	@Override
	public int hashCode() {		
		int result = 1;
		result = result + ((first == null) ? 0 : first.hashCode());
		result = result + ((second == null) ? 0 : second.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Route other = (Route) obj;
		//graf nieskierowany, kolejnosc wierzcholkow nie ma znaczenia
		if((first.equals(other.first) && second.equals(other.second))
				|| (first.equals(other.second) && second.equals(other.first))){
			return true;
		}
		return false;
	}

}
