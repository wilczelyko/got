package com.dalu.games.gameOfThrones.house.cards;

import java.util.HashMap;
import java.util.Map;

import com.dalu.games.gameOfThrones.house.cards.greyjoy.BalonGreyjoy;
import com.dalu.games.gameOfThrones.house.cards.greyjoy.DagmaerCleftjaw;
import com.dalu.games.gameOfThrones.house.cards.greyjoy.EuronCrowsEye;
import com.dalu.games.gameOfThrones.house.cards.stark.EddardStark;
import com.dalu.games.gameOfThrones.house.cards.stark.RobbStark;
import com.dalu.games.gameOfThrones.house.cards.stark.SerRodrickCassel;
import com.dalu.games.gameOfThrones.main.GameplayManager;

public class CardFactory {
	private static Map<String, Class<? extends Card>> cards;

	static {
		cards = new HashMap<String, Class<? extends Card>>();
		cards.put("Eddard Stark", EddardStark.class);
		cards.put("Ser Rodrick Cassel", SerRodrickCassel.class);
		cards.put("Robb Stark", RobbStark.class);
		
		cards.put("Euron Crow's Eye", EuronCrowsEye.class);
		cards.put("Dagmaer Cleftjaw", DagmaerCleftjaw.class);
		cards.put("Balon Greyjoy", BalonGreyjoy.class);
	}

	public static Card createCard(String string, GameplayManager gameplayManager) {
		Class<? extends Card> cardCalss = cards.get(string);
		if (cardCalss == null) {
			throw new IllegalStateException("No card with name " + string + " found. Names:" + cards.keySet());
		}
		try {
			return cardCalss.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new IllegalStateException("No card with name " + string + " found. Names:" + cards.keySet());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new IllegalStateException("No card with name " + string + " found. Names:" + cards.keySet());
		}
	}

}
