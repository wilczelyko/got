package com.dalu.games.gameOfThrones.house.cards;

import com.dalu.games.gameOfThrones.main.Battle;

public abstract class Card {
	private int strength;
	private int swords;
	private int towers;
	
	public abstract void specialAction(Battle battle);

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getSwords() {
		return swords;
	}

	public void setSwords(int swords) {
		this.swords = swords;
	}

	public int getTowers() {
		return towers;
	}

	public void setTowers(int towers) {
		this.towers = towers;
	}
}
