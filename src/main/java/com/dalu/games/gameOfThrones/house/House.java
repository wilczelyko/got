package com.dalu.games.gameOfThrones.house;

import java.util.LinkedList;
import java.util.List;

import com.dalu.games.gameOfThrones.house.cards.Card;
import com.dalu.games.gameOfThrones.map.Region;
import com.dalu.games.gameOfThrones.orders.OrdersPool;
import com.dalu.games.gameOfThrones.units.Unit;
import com.dalu.games.gameOfThrones.units.UnitsPool;

public class House {
	private String familyName;
	private List<Route> routes;
	private List<Card> availableCards;
	private List<Card> usedCards;
	private UnitsPool unitsPool;
	private List<Region> controlledRegions;
	private List<Unit> unitsOnMap;
	private Region homeLand;
	private OrdersPool ordersPool;
	private int powerTokens;

	public List<Region> getOrderableRegions() {
		List<Region> orderableRegions = new LinkedList<Region>();
		for (Unit unit : unitsOnMap) {
			if (!orderableRegions.contains(unit.getRegion())) {
				orderableRegions.add(unit.getRegion());
			}
		}
		return orderableRegions;
	}

	public void removePowerTokens(int amount) {
		if (powerTokens - amount >= 0) {
			powerTokens = powerTokens - amount;
		} else {
			powerTokens = 0;
		}
	}

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

	public List<Card> getAvailableCards() {
		return availableCards;
	}

	public void setAvailableCards(List<Card> availableCards) {
		this.availableCards = availableCards;
	}

	public List<Card> getUsedCards() {
		return usedCards;
	}

	public void setUsedCards(List<Card> usedCards) {
		this.usedCards = usedCards;
	}

	public UnitsPool getUnitsPool() {
		return unitsPool;
	}

	public void setUnitsPool(UnitsPool unitsPool) {
		this.unitsPool = unitsPool;
	}

	public List<Region> getControlledRegions() {
		return controlledRegions;
	}

	public void setControlledRegions(List<Region> controlledRegions) {
		this.controlledRegions = controlledRegions;
	}

	public List<Unit> getUnitsOnMap() {
		return unitsOnMap;
	}

	public void setUnitsOnMap(List<Unit> unitsOnMap) {
		this.unitsOnMap = unitsOnMap;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public Region getHomeLand() {
		return homeLand;
	}

	public void setHomeLand(Region homeLand) {
		this.homeLand = homeLand;
	}

	public OrdersPool getOrdersPool() {
		return ordersPool;
	}

	public void setOrdersPool(OrdersPool ordersPool) {
		this.ordersPool = ordersPool;
	}

	public int getPowerTokens() {
		return powerTokens;
	}

	public void setPowerTokens(int powerTokens) {
		this.powerTokens = powerTokens;
	}
}
