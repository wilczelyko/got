package com.dalu.games.gameOfThrones.dao.converters;

import java.util.HashMap;

import java.util.Map;

import com.dalu.games.gameOfThrones.dao.generated.LandRegionEntity;
import com.dalu.games.gameOfThrones.dao.generated.PortRegionEntity;
import com.dalu.games.gameOfThrones.dao.generated.RegionEntity;
import com.dalu.games.gameOfThrones.dao.generated.SeaRegionEntity;

public class RegionConverterFactory {
	private static Map<Class<? extends RegionEntity>,  RegionConverter> converters;
	static {
		converters = new HashMap<Class<? extends RegionEntity>,  RegionConverter>();
		converters.put(LandRegionEntity.class, new LandRegionConverter());
		converters.put(SeaRegionEntity.class, new SeaRegionConverter());
		converters.put(PortRegionEntity.class, new PortRegionConverter());
	}
	
	public static RegionConverter<? super RegionEntity> getConverter(Class<? extends RegionEntity> class1){
		return converters.get(class1);
	}
}
