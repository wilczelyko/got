package com.dalu.games.gameOfThrones.dao.converters;

import java.util.Map;

import com.dalu.games.gameOfThrones.dao.generated.RouteEntity;
import com.dalu.games.gameOfThrones.house.Route;
import com.dalu.games.gameOfThrones.map.Region;

public class RouteConverter {

	public Route convertRouteEntityToRoute(RouteEntity routeEntity, Map<String, Region> map) {
		if(routeEntity.getFirstNode().equals(routeEntity.getSecondNode())){
			throw new IllegalStateException("Route has same start and end = "+routeEntity.getFirstNode());
		}
		Region firstRegion = map.get(routeEntity.getFirstNode());
		if (firstRegion == null) {
			throw new IllegalStateException("First node of route not found in regions map. Node="
					+ routeEntity.getFirstNode() + ". Map=" + map);
		}
		Region secondRegion = map.get(routeEntity.getSecondNode());
		if (secondRegion == null) {
			throw new IllegalStateException("First node of route not found in regions map. Node="
					+ routeEntity.getSecondNode() + ". Map=" + map);
		}
		Route route = new Route(firstRegion, secondRegion);
		return route;
	}

}
