package com.dalu.games.gameOfThrones.dao.converters;

import com.dalu.games.gameOfThrones.dao.generated.LandRegionEntity;
import com.dalu.games.gameOfThrones.map.Land;
import com.dalu.games.gameOfThrones.map.Region;

public class LandRegionConverter extends RegionConverter<LandRegionEntity> {
	public Region convertRegionEntityToRegion(LandRegionEntity regionEntity) {
		Land land = new Land();
		land.setCastles(regionEntity.getCastles());
		land.setKeeps(regionEntity.getKeeps());
		land.setPower(regionEntity.getPower());
		land.setResources(regionEntity.getResources());
		copyCommonValues(regionEntity, land);
		return land;
	}
}
