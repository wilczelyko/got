package com.dalu.games.gameOfThrones.dao.converters;

import com.dalu.games.gameOfThrones.dao.generated.CardEntity;
import com.dalu.games.gameOfThrones.house.cards.Card;
import com.dalu.games.gameOfThrones.house.cards.CardFactory;
import com.dalu.games.gameOfThrones.main.GameplayManager;

public class CardsConverter {
	public Card convertCardEntityToCard(CardEntity cardEntity, GameplayManager gameplayManager){
		Card card = CardFactory.createCard(cardEntity.getName(), gameplayManager);
		card.setStrength(cardEntity.getStrenght());
		card.setSwords(cardEntity.getSwords());
		card.setTowers(cardEntity.getTowers());
		return card;
	}
}
