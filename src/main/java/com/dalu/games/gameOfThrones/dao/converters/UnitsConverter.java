package com.dalu.games.gameOfThrones.dao.converters;

import java.util.Map;

import com.dalu.games.gameOfThrones.dao.generated.UnitEntity;
import com.dalu.games.gameOfThrones.dao.generated.UnitTypeEnum;
import com.dalu.games.gameOfThrones.map.Region;
import com.dalu.games.gameOfThrones.units.Knight;
import com.dalu.games.gameOfThrones.units.Ship;
import com.dalu.games.gameOfThrones.units.SidgeTower;
import com.dalu.games.gameOfThrones.units.Swordsman;
import com.dalu.games.gameOfThrones.units.Unit;

public class UnitsConverter {

	public Unit convertUnitEntityToUnit(UnitEntity unitEntity, Map<String, Region> map) {
		Unit unit = null;
		Region region = map.get(unitEntity.getRegion());
		if (region == null) {
			throw new IllegalStateException("No region " + unitEntity.getRegion() + " in regions map:" + map
					+ " for unit.");
		}
		if (UnitTypeEnum.SHIP.equals(unitEntity.getUnitType())) {
			unit = new Ship();
		} else if (UnitTypeEnum.SWORDSMAN.equals(unitEntity.getUnitType())) {
			unit = new Swordsman();
		} else if (UnitTypeEnum.KNIGHT.equals(unitEntity.getUnitType())) {
			unit = new Knight();
		} else if (UnitTypeEnum.SIDGETOWER.equals(unitEntity.getUnitType())) {
			unit = new SidgeTower();
		}
		unit.setRegion(region);
		region.getUnits().add(unit);
		return unit;
	}

}
