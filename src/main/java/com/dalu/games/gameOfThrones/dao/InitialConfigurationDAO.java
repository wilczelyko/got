package com.dalu.games.gameOfThrones.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.dalu.games.gameOfThrones.dao.generated.HouseEntity;
import com.dalu.games.gameOfThrones.dao.generated.InitialConfigurationEntity;
import com.dalu.games.gameOfThrones.dao.generated.ObjectFactory;
import com.dalu.games.gameOfThrones.dao.generated.RegionEntity;

public class InitialConfigurationDAO {
	public List<RegionEntity> loadRegions() {

		InitialConfigurationEntity initialConfigurationEntity = readXml();
		List<RegionEntity> regions = new ArrayList<RegionEntity>(initialConfigurationEntity.getLandRegion().size()
				+ initialConfigurationEntity.getSeaRegion().size() + initialConfigurationEntity.getPortRegion().size());
		regions.addAll(initialConfigurationEntity.getLandRegion());
		regions.addAll(initialConfigurationEntity.getSeaRegion());
		regions.addAll(initialConfigurationEntity.getPortRegion());
		initialConfigurationEntity.getHouse();
		return regions;
	}
	
	public List<HouseEntity> loadHouses(){
		InitialConfigurationEntity initialConfigurationEntity = readXml();
		List<HouseEntity> houses = new ArrayList<HouseEntity>(initialConfigurationEntity.getHouse().size());
		houses.addAll(initialConfigurationEntity.getHouse());		
		return houses;
	}

	@SuppressWarnings("unchecked")
	private InitialConfigurationEntity readXml() {
		JAXBElement<InitialConfigurationEntity> jaxbElement = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(ObjectFactory.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			File xml = new File("conf/InitialConfiguration.xml");
			jaxbElement = (JAXBElement<InitialConfigurationEntity>) unmarshaller.unmarshal(xml);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		InitialConfigurationEntity initialConfigurationEntity = jaxbElement.getValue();
		return initialConfigurationEntity;
	}
	
	

}
