package com.dalu.games.gameOfThrones.dao.converters;

import com.dalu.games.gameOfThrones.dao.generated.RegionEntity;
import com.dalu.games.gameOfThrones.map.Region;

public abstract class RegionConverter<T extends RegionEntity> {
	 public abstract Region convertRegionEntityToRegion(T  regionEntity);
	 
		protected void copyCommonValues(RegionEntity from, Region to){
			to.setCode(from.getCode());		
		}
}
