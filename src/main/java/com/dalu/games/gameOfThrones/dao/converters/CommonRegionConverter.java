package com.dalu.games.gameOfThrones.dao.converters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.dalu.games.gameOfThrones.dao.generated.RegionEntity;
import com.dalu.games.gameOfThrones.map.Region;

public class CommonRegionConverter {
	public static void fillNeighbors(List<RegionEntity> regionEntities, Map<String, Region> regions) {
		Map<String, Region> regionsMap = regions;
		for (RegionEntity regionEntity : regionEntities) {
			Region region = regionsMap.get(regionEntity.getCode());
			if (region != null) {
				List<Region> neighbors = new ArrayList<Region>();
				for (String neighborCode : regionEntity.getNeighborCode()) {
					Region neighbor = regionsMap.get(neighborCode);
					if (neighbor != null) {
						neighbors.add(neighbor);
					} else {
						throw new IllegalStateException("Cannot find neighbor with code " + regionEntity.getCode());
					}
				}
				region.setNeighbors(neighbors);
			} else {
				throw new IllegalStateException("Cannot find region with code " + regionEntity.getCode());
			}
		}
	}
}
