package com.dalu.games.gameOfThrones.dao.converters;

import com.dalu.games.gameOfThrones.dao.generated.SeaRegionEntity;
import com.dalu.games.gameOfThrones.map.Region;
import com.dalu.games.gameOfThrones.map.Sea;

public class SeaRegionConverter extends RegionConverter<SeaRegionEntity> {

	public Region convertRegionEntityToRegion(SeaRegionEntity regionEntity) {
		Sea sea = new Sea();
		copyCommonValues(regionEntity, sea);
		return sea;
	}

}
