package com.dalu.games.gameOfThrones.dao.converters;

import com.dalu.games.gameOfThrones.dao.generated.PortRegionEntity;
import com.dalu.games.gameOfThrones.map.Port;
import com.dalu.games.gameOfThrones.map.Region;

public class PortRegionConverter extends RegionConverter<PortRegionEntity> {

	public Region convertRegionEntityToRegion(PortRegionEntity regionEntity) {
		Port port = new Port();
		copyCommonValues(regionEntity, port);
		return port;
	}

}
