package com.dalu.games.gameOfThrones.dao.converters;

import com.dalu.games.gameOfThrones.dao.generated.RegionEntity;
import com.dalu.games.gameOfThrones.map.Region;

public abstract class RegionConverterAbstract {
	protected void copyCommonValues(RegionEntity from, Region to){
		to.setCode(from.getCode());		
	}
}
