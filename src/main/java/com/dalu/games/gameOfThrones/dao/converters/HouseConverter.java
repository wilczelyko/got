package com.dalu.games.gameOfThrones.dao.converters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.dalu.games.gameOfThrones.dao.generated.CardEntity;
import com.dalu.games.gameOfThrones.dao.generated.HouseEntity;
import com.dalu.games.gameOfThrones.dao.generated.RouteEntity;
import com.dalu.games.gameOfThrones.dao.generated.UnitEntity;
import com.dalu.games.gameOfThrones.dao.generated.UnitsPoolEntity;
import com.dalu.games.gameOfThrones.house.House;
import com.dalu.games.gameOfThrones.house.Route;
import com.dalu.games.gameOfThrones.house.cards.Card;
import com.dalu.games.gameOfThrones.main.GameplayManager;
import com.dalu.games.gameOfThrones.main.InfluenceTracks;
import com.dalu.games.gameOfThrones.map.Region;
import com.dalu.games.gameOfThrones.units.Unit;
import com.dalu.games.gameOfThrones.units.UnitsPool;

public class HouseConverter {
	public House convertHouseEntityToHouse(HouseEntity houseEntity, GameplayManager gameplayManager) {
		House house = new House();
		house.setFamilyName(houseEntity.getFamilyName());

		// routes of house
		List<Route> routes = new ArrayList<Route>(houseEntity.getRoutes().size());
		house.setRoutes(routes);
		for (RouteEntity routeEntity : houseEntity.getRoutes()) {
			Route route = (new RouteConverter()).convertRouteEntityToRoute(routeEntity, gameplayManager.getGame()
					.getRegions());
			routes.add(route);
		}

		// units available to muster
		UnitsPoolEntity unitsPoolEntity = houseEntity.getUnitsPool();
		UnitsPool unitsPool = new UnitsPool(unitsPoolEntity.getShips(), unitsPoolEntity.getSwordsman(),
				unitsPoolEntity.getKnights(), unitsPoolEntity.getSiedgeTowers());
		house.setUnitsPool(unitsPool);

		// units on map
		List<Unit> units = new ArrayList<Unit>();
		house.setUnitsOnMap(units);
		UnitsConverter unitsConverter = new UnitsConverter();
		for (UnitEntity unitEntity : houseEntity.getUnitsOnMap()) {
			Unit unit = unitsConverter.convertUnitEntityToUnit(unitEntity, gameplayManager.getGame().getRegions());
			unit.setHouse(house);
			units.add(unit);
		}

		// available cards
		List<Card> availableCards = new ArrayList<Card>();
		house.setAvailableCards(availableCards);
		CardsConverter cardsConverter = new CardsConverter();
		for (CardEntity cardEntity : houseEntity.getAvailableCards()) {
			Card avaialbleCard = cardsConverter.convertCardEntityToCard(cardEntity, gameplayManager);
			availableCards.add(avaialbleCard);
		}

		// used cards;
		List<Card> usedleCards = new ArrayList<Card>();
		house.setUsedCards(usedleCards);
		for (CardEntity cardEntity : houseEntity.getUsedCards()) {
			Card usedCards = cardsConverter.convertCardEntityToCard(cardEntity, gameplayManager);
			usedleCards.add(usedCards);
		}

		// home land
		Region homeland = gameplayManager.getGame().getRegions().get(houseEntity.getHomeLand());
		if (homeland == null) {
			throw new IllegalStateException("Cannot find homeland " + houseEntity.getHomeLand() + " in regions:"
					+ gameplayManager.getGame().getRegions());
		}
		house.setHomeLand(homeland);
		return house;
	}

	public void fillInfluenceTracks(List<HouseEntity> houseEntities, Map<String, House> houses,
			InfluenceTracks influenceTracks) {
		int maxSize = 50;
		House[] theIronThrone = new House[maxSize];
		House[] theFieldoms = new House[maxSize];
		House[] theKingsCourt = new House[maxSize];
		for (HouseEntity houseEntity : houseEntities) {
			House house = houses.get(houseEntity.getFamilyName());
			if (house == null) {
				throw new IllegalStateException("Cannot find house " + houseEntity.getFamilyName() + " in houses "
						+ houses);
			}
			theIronThrone[houseEntity.getTheIronThroneTrackPosition()] = house;
			theFieldoms[houseEntity.getTheFieldomsTrackPosition()] = house;
			theKingsCourt[houseEntity.getTheKingsCourtTrackPosition()] = house;
		}
		for (int i = 0; i < maxSize; i++) {
			if (theIronThrone[i] != null) {
				influenceTracks.getTheIronThrone().add(theIronThrone[i]);
			}
			if (theFieldoms[i] != null) {
				influenceTracks.getTheFieldoms().add(theFieldoms[i]);
			}
			if (theKingsCourt[i] != null) {
				influenceTracks.getTheKingsCourt().add(theKingsCourt[i]);
			}
		}

	}

}
