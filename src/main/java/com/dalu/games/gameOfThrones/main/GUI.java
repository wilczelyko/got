package com.dalu.games.gameOfThrones.main;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Scanner;

public class GUI {
	private static ResourceBundle bundle = ResourceBundle.getBundle("messages");
	private Scanner scanner = new Scanner(System.in);

	public String whereToMove(String unit, String code, List<String> reachableRegionStrings) {
		String message = String.format(bundle.getString("move.whereToMove"), bundle.getString(unit),
				bundle.getString("region." +code));
		System.out.println(message);
		
		String response = chooseRegion(reachableRegionStrings, "move.reachableRegions");
		return response;
	}

	public void displayWhoIsSetingOrders(String familyName) {
		System.out
				.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		String message = String.format(bundle.getString("planing.turnOfFamily"), familyName);
		System.out.println(message);
		scanner.nextLine();		
	}

	public String askForOrderToRegion(String region, List<String> availableOrdersString) {
		String availableOrder = "[";
		for (String string : availableOrdersString) {
			availableOrder = availableOrder + string + ",";
		}
		availableOrder = availableOrder.substring(0, availableOrder.length() - 1);
		availableOrder = availableOrder + "]";
		String message = String.format(bundle.getString("planing.askForOrderToRegion"),
				bundle.getString("region." + region), availableOrder);
		System.out.println(message);
		String response = scanner.nextLine();
		return response;
	}

	public void closeScanner() {
		scanner.close();
	}

	public void showOrdersToAllPlayers(Map<String, List<OrderAndRegionString>> assignedordersToDisplay) {
		System.out
				.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		for (Entry<String, List<OrderAndRegionString>> entry : assignedordersToDisplay.entrySet()) {
			String message = String.format(bundle.getString("planing.ordersForFamily"), entry.getKey());
			System.out.println(message);

			for (OrderAndRegionString order : entry.getValue()) {
				message = bundle.getString("region." + order.regionCode) + " - " + order.orderCode;
				System.out.println(message);
			}
		}
		System.out.println("*****************************************************");
	}

	public String askForRegionToExecuteOrder(List<String> regionsToChoose) {
		return chooseRegion(regionsToChoose, "action.chooseOrderToExecute");	
	}

	public String askToChooseRegions(List<String> regionsToChoose) {
		return chooseRegion(regionsToChoose, "action.chooseRegionToRaid");		
	}

	private String chooseRegion(List<String> regionsToChoose, String initialMessage) {
		System.out.print(bundle.getString(initialMessage));
		for (String regionCode : regionsToChoose) {
			System.out.print(bundle.getString("region."+regionCode) + ",");
		}
		System.out.println();
		String response = scanner.nextLine();
		return response;
	}
}
