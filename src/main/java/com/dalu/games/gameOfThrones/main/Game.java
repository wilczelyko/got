package com.dalu.games.gameOfThrones.main;

import java.util.HashMap;
import java.util.Map;

import com.dalu.games.gameOfThrones.house.House;
import com.dalu.games.gameOfThrones.map.Region;

public class Game {
	
	private Map<String, Region> regions = new HashMap<String, Region>();
	private Map<String, House> houses = new HashMap<String, House>();
	private InfluenceTracks influenceTracks = new InfluenceTracks();
	private int round;
	

	public Map<String, Region> getRegions() {
		return regions;
	}

	public void setRegions(Map<String, Region> regions) {
		this.regions = regions;
	}

	public Map<String, House> getHouses() {
		return houses;
	}

	public void setHouses(Map<String, House> houses) {
		this.houses = houses;
	}

	public InfluenceTracks getInfluenceTracks() {
		return influenceTracks;
	}

	public void setInfluenceTracks(InfluenceTracks influenceTracks) {
		this.influenceTracks = influenceTracks;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}	
}
