package com.dalu.games.gameOfThrones.main;

import java.util.ArrayList;
import java.util.List;

import com.dalu.games.gameOfThrones.house.House;

public class InfluenceTracks {
	private List<House> theIronThrone = new ArrayList<House>();
	private List<House> theFieldoms = new ArrayList<House>();
	private List<House> theKingsCourt = new ArrayList<House>();
	public List<House> getTheIronThrone() {
		return theIronThrone;
	}
	public void setTheIronThrone(List<House> theIronThrone) {
		this.theIronThrone = theIronThrone;
	}
	public List<House> getTheFieldoms() {
		return theFieldoms;
	}
	public void setTheFieldoms(List<House> theFieldoms) {
		this.theFieldoms = theFieldoms;
	}
	public List<House> getTheKingsCourt() {
		return theKingsCourt;
	}
	public void setTheKingsCourt(List<House> theKingsCourt) {
		this.theKingsCourt = theKingsCourt;
	}

	
}
