package com.dalu.games.gameOfThrones.main;

import com.dalu.games.gameOfThrones.dao.InitialConfigurationDAO;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       GameplayManager gameplayManager = new GameplayManager();
       GUI gui = new GUI();
       gameplayManager.setGui(gui);
       InitialConfigurationDAO initialConfigurationDAO = new InitialConfigurationDAO();
       gameplayManager.setInitialConfigurationDAO(initialConfigurationDAO);       
       gameplayManager.runGame();
    }
}
