package com.dalu.games.gameOfThrones.main;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.sun.istack.logging.Logger;
import com.dalu.games.gameOfThrones.dao.InitialConfigurationDAO;
import com.dalu.games.gameOfThrones.dao.converters.CommonRegionConverter;
import com.dalu.games.gameOfThrones.dao.converters.HouseConverter;
import com.dalu.games.gameOfThrones.dao.converters.RegionConverterFactory;
import com.dalu.games.gameOfThrones.dao.generated.HouseEntity;
import com.dalu.games.gameOfThrones.dao.generated.RegionEntity;
import com.dalu.games.gameOfThrones.house.House;
import com.dalu.games.gameOfThrones.map.Region;
import com.dalu.games.gameOfThrones.orders.ConsolidatePowerOrder;
import com.dalu.games.gameOfThrones.orders.DefenseOrder;
import com.dalu.games.gameOfThrones.orders.MarchOrder;
import com.dalu.games.gameOfThrones.orders.Order;
import com.dalu.games.gameOfThrones.orders.OrdersPool;
import com.dalu.games.gameOfThrones.orders.RaidOrder;
import com.dalu.games.gameOfThrones.orders.SuportOrder;
import com.dalu.games.gameOfThrones.orders.executors.MarchOrderExecutor;
import com.dalu.games.gameOfThrones.orders.executors.RaidOrderExecutor;
import com.dalu.games.gameOfThrones.units.Unit;

public class GameplayManager {
	public GUI gui;
	private Game game;
	private InitialConfigurationDAO initialConfigurationDAO;

	public Region whereToMove(Unit unit, Region departureRegion, List<Region> reachableRegions) {
		List<String> reachableRegionStrings = new LinkedList<String>();
		for (Region region : reachableRegions) {
			reachableRegionStrings.add(region.getCode());
		}
		String whereTo = gui.whereToMove(unit.getResourceBoundle(), departureRegion.getCode(), reachableRegionStrings);
		Region region = game.getRegions().get(whereTo);
		return region;
	}

	public void runGame() {
		loadInitialMap();
		for (int i = 1; i <= 10; i++) {
			executeRound(i);
		}
		gui.closeScanner();
	}

	private void executeRound(int round) {
		if (round != 1) {
			executeWesterosPahse();
		}
		executePlaningPhase();
		executeActionPhase();
	}

	private void executeActionPhase() {
		executeRaidOrders();
		executeMovmentOrders();
	}

	private void executeRaidOrders() {
		new RaidOrderExecutor(gui, game).execute();
	}

	private void executeMovmentOrders() {
		new MarchOrderExecutor(gui, game).execute();
	}

	private void executePlaningPhase() {
		Map<String, List<OrderAndRegionString>> assignedordersToDisplay = new HashMap<String, List<OrderAndRegionString>>();
		for (House house : game.getInfluenceTracks().getTheIronThrone()) {
			gui.displayWhoIsSetingOrders(house.getFamilyName());
			List<Region> orderableRegions = house.getOrderableRegions();
			OrdersPool ordersPool = house.getOrdersPool();
			ordersPool.resetOrders(getAvailableStarOrders(house));

			List<OrderAndRegionString> assignedOrdersOfFamily = new LinkedList<OrderAndRegionString>();
			assignedordersToDisplay.put(house.getFamilyName(), assignedOrdersOfFamily);
			for (Region region : orderableRegions) {
				List<String> availableOrdersString = ordersPool.getAvailableOrdersString();
				String orderCode = gui.askForOrderToRegion(region.getCode(), availableOrdersString);
				Order order = ordersPool.getOrderTypesMap().get(orderCode);
				if (order != null) {
					ordersPool.assignOrder(order, region);
					assignedOrdersOfFamily.add(new OrderAndRegionString(region.getCode(), order.getCode()));
				} else {
					Logger.getLogger(GameplayManager.class).warning("Cannot find order with given code:" + orderCode);
				}
			}

		}
		gui.showOrdersToAllPlayers(assignedordersToDisplay);
	}

	public Region chooseRegion(List<Region> raidableRegions) {
		List<String> regionsString = new LinkedList<String>();
		for (Region region : raidableRegions) {
			regionsString.add(region.getCode());
		}
		String choice = gui.askToChooseRegions(regionsString);
		Region region = game.getRegions().get(choice);
		return region;
	}

	private void executeWesterosPahse() {
		// TODO Auto-generated method stub

	}

	private void loadInitialMap() {
		game = new Game();
		game.setRegions(new HashMap<String, Region>());
		List<RegionEntity> regionEntities = initialConfigurationDAO.loadRegions();
		for (RegionEntity regionEntity : regionEntities) {
			Region region = RegionConverterFactory.getConverter(regionEntity.getClass()).convertRegionEntityToRegion(
					regionEntity);
			game.getRegions().put(region.getCode(), region);
		}
		CommonRegionConverter.fillNeighbors(regionEntities, game.getRegions());

		game.setHouses(new HashMap<String, House>());
		List<HouseEntity> houseEntities = initialConfigurationDAO.loadHouses();
		HouseConverter houseConverter = new HouseConverter();
		for (HouseEntity houseEntity : houseEntities) {
			House house = houseConverter.convertHouseEntityToHouse(houseEntity, this);
			game.getHouses().put(house.getFamilyName(), house);
		}
		InfluenceTracks influenceTracks = new InfluenceTracks();
		game.setInfluenceTracks(influenceTracks);
		houseConverter.fillInfluenceTracks(houseEntities, game.getHouses(), influenceTracks);

		initiateOrdersPools();

		game.setRound(1);
	}

	private void initiateOrdersPools() {
		for (Entry<String, House> entry : game.getHouses().entrySet()) {
			House house = entry.getValue();
			OrdersPool ordersPool = new OrdersPool();
			ordersPool.setHouse(house);
			Map<String, Order> ordersMap = new HashMap<String, Order>();
			ordersMap.put("M0", new MarchOrder(0, this, "M0", false));
			ordersMap.put("M-1", new MarchOrder(-1, this, "M-1", false));
			ordersMap.put("M+1", new MarchOrder(1, this, "M+1", true));

			ordersMap.put("D+1a", new DefenseOrder(1, this, "D+1a", false));
			ordersMap.put("D+1b", new DefenseOrder(1, this, "D+1b", false));
			ordersMap.put("D+2", new DefenseOrder(2, this, "D+2", true));

			ordersMap.put("S0a", new SuportOrder(0, this, "S0a", false));
			ordersMap.put("S0b", new SuportOrder(0, this, "S0b", false));
			ordersMap.put("S+1", new SuportOrder(1, this, "S+1", true));

			ordersMap.put("Ra", new RaidOrder(this, "Ra", false));
			ordersMap.put("Rb", new RaidOrder(this, "Rb", false));
			ordersMap.put("R*", new RaidOrder(this, "R*", true));

			ordersMap.put("CPa", new ConsolidatePowerOrder(this, "CPa", false));
			ordersMap.put("CPb", new ConsolidatePowerOrder(this, "CPb", false));
			ordersMap.put("CP*", new ConsolidatePowerOrder(this, "CP*", true));

			ordersPool.setOrderTypesMap(ordersMap);
			ordersPool.initialize(getAvailableStarOrders(house));
			house.setOrdersPool(ordersPool);
		}
	}

	private Integer getAvailableStarOrders(House house) {
		return OrdersPool.starOrdersForCourtPosition
				.get(game.getInfluenceTracks().getTheKingsCourt().indexOf(house) + 1);
	}

	public void setGui(GUI gui) {
		this.gui = gui;
	}

	public InitialConfigurationDAO getInitialConfigurationDAO() {
		return initialConfigurationDAO;
	}

	public void setInitialConfigurationDAO(InitialConfigurationDAO initialConfigurationDAO) {
		this.initialConfigurationDAO = initialConfigurationDAO;
	}

	public Game getGame() {
		return game;
	}
}
