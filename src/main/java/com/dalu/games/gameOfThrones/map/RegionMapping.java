package com.dalu.games.gameOfThrones.map;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class RegionMapping {
	
	public static final Map<String, String> shortcutToName;
	public static final Map<String, String> nameToShortcut;
	private static ResourceBundle bundle = ResourceBundle.getBundle("messages");
	static{
		shortcutToName = new HashMap<String, String>();
		nameToShortcut = new HashMap<String, String>();
		shortcutToName.put("WF", bundle.getString("WF"));
		nameToShortcut.put(bundle.getString("WF"), "WF");
		shortcutToName.put(bundle.getString("WH"),"WH");
		
	}

}
