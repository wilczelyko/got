package com.dalu.games.gameOfThrones.map;

public class Land extends Region {
	protected int power;
	protected int resources;
	protected int castles;
	protected int keeps;

	@Override
	public boolean isLand() {
		return true;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public int getResources() {
		return resources;
	}

	public void setResources(int resources) {
		this.resources = resources;
	}

	public int getCastles() {
		return castles;
	}

	public void setCastles(int castles) {
		this.castles = castles;
	}

	public int getKeeps() {
		return keeps;
	}

	public void setKeeps(int keeps) {
		this.keeps = keeps;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + code.hashCode();
		result = prime * result + castles;
		result = prime * result + keeps;
		result = prime * result + power;
		result = prime * result + resources;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Land other = (Land) obj;
		if (code != other.getCode())
			return false;
		if (castles != other.castles)
			return false;
		if (keeps != other.keeps)
			return false;
		if (power != other.power)
			return false;
		if (resources != other.resources)
			return false;
		return true;
	}
}
