package com.dalu.games.gameOfThrones.map;

import java.util.LinkedList;
import java.util.List;

import com.dalu.games.gameOfThrones.orders.Order;
import com.dalu.games.gameOfThrones.units.Unit;

public abstract class Region {

	protected String code;
	protected List<Unit> units = new LinkedList<Unit>();
	protected List<Region> neighbors;
	protected Order order;
	
	public abstract boolean isLand();
	
	public boolean isNeighbord(Region region) {
		return neighbors.contains(region);
	}
	
	public List<Unit> getUnits() {
		return units;
	}
	public void setUnits(List<Unit> units) {
		this.units = units;
	}
	public List<Region> getNeighbors() {
		return neighbors;
	}
	public void setNeighbors(List<Region> neighbors) {
		this.neighbors = neighbors;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Region other = (Region) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}	
	
}
