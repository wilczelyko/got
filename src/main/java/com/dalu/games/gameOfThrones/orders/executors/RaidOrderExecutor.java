package com.dalu.games.gameOfThrones.orders.executors;

import com.dalu.games.gameOfThrones.main.GUI;
import com.dalu.games.gameOfThrones.main.Game;
import com.dalu.games.gameOfThrones.orders.Order;
import com.dalu.games.gameOfThrones.orders.RaidOrder;

public class RaidOrderExecutor extends OrderExecutor {
	public RaidOrderExecutor(GUI gui, Game game) {
		this.game = game;
		this.gui = gui;
	}
	boolean isOrderOfType(Order order) {
		return order instanceof RaidOrder;
	}

}
