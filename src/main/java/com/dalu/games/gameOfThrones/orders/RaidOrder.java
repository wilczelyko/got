package com.dalu.games.gameOfThrones.orders;

import java.util.LinkedList;
import java.util.List;

import com.dalu.games.gameOfThrones.main.GameplayManager;
import com.dalu.games.gameOfThrones.map.Region;
import com.sun.istack.logging.Logger;

public class RaidOrder extends Order {

	public RaidOrder(GameplayManager gameplayManager, String code, boolean starOrder) {
		super(gameplayManager, code, starOrder);
	}

	@Override
	public void execute() {
		List<Region> raidableRegions = new LinkedList<Region>();
		for (Region regionNeigbor : region.getNeighbors()) {
			Order neighborOrder = regionNeigbor.getOrder();
			if (neighborOrder != null
					&& !neighborOrder.getOrdersPool().getHouse().equals(ordersPool.getHouse())
					&& (!region.isLand() || regionNeigbor.isLand())
					&& (neighborOrder instanceof SuportOrder || neighborOrder instanceof RaidOrder
							|| neighborOrder instanceof ConsolidatePowerOrder || (starOrder && neighborOrder instanceof DefenseOrder))) {
				raidableRegions.add(regionNeigbor);
			}
			if (raidableRegions.size() > 0) {
				Region choice = gameplayManager.chooseRegion(raidableRegions);
				if (raidableRegions.contains(choice)) {
					choice.getOrder().getOrdersPool().returnOrder(choice.getOrder());
					if (choice.getOrder() instanceof ConsolidatePowerOrder) {
						ordersPool.getHouse().setPowerTokens(ordersPool.getHouse().getPowerTokens() + 1);
						choice.getOrder().getOrdersPool().getHouse().removePowerTokens(1);
					}
				} else {
					Logger.getLogger(RaidOrder.class).warning(
							"User tries to rain unreachable region:" + choice.getCode());
				}
			}
			ordersPool.returnOrder(this);
		}

	}

}
