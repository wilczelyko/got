package com.dalu.games.gameOfThrones.orders.executors;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.dalu.games.gameOfThrones.house.House;
import com.dalu.games.gameOfThrones.main.GUI;
import com.dalu.games.gameOfThrones.main.Game;
import com.dalu.games.gameOfThrones.map.Region;
import com.dalu.games.gameOfThrones.orders.Order;

public abstract class OrderExecutor {
	protected GUI gui;
	protected Game game;

	public void execute() {
		boolean ordersFound = true;
		while (ordersFound) {
			ordersFound = false;
			for (House house : game.getInfluenceTracks().getTheIronThrone()) {
				List<Order> executableOrders = getUsedOrders(house);
				if (executableOrders.size() > 0) {
					ordersFound = true;
					List<String> regionsToChoose = new ArrayList<String>(executableOrders.size());
					for (Order order : executableOrders) {
						regionsToChoose.add(order.getRegion().getCode());
					}
					String regionCode = gui.askForRegionToExecuteOrder(regionsToChoose);
					Region region = game.getRegions().get(regionCode);
					region.getOrder().execute();
				}
			}
		}
	}
	
	public List<Order> getUsedOrders(House house){
		List<Order> executableRaidOrders = new LinkedList<Order>();
		for (Order order : house.getOrdersPool().getUsedOrders()) {
			if (isOrderOfType(order) && order.getRegion() != null) {
				executableRaidOrders.add(order);
			}
		}
		return executableRaidOrders;
	}

	abstract boolean isOrderOfType(Order order) ;
	
	

}
