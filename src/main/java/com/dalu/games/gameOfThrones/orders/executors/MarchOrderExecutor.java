package com.dalu.games.gameOfThrones.orders.executors;

import com.dalu.games.gameOfThrones.main.GUI;
import com.dalu.games.gameOfThrones.main.Game;
import com.dalu.games.gameOfThrones.orders.MarchOrder;
import com.dalu.games.gameOfThrones.orders.Order;

public class MarchOrderExecutor extends OrderExecutor {
	public MarchOrderExecutor(GUI gui, Game game) {
		this.game = game;
		this.gui = gui;
	}

	boolean isOrderOfType(Order order) {
		return order instanceof MarchOrder;
	}
}
