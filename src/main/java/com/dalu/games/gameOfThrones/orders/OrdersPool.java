package com.dalu.games.gameOfThrones.orders;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.dalu.games.gameOfThrones.house.House;
import com.dalu.games.gameOfThrones.map.Region;

public class OrdersPool {
	public static final Map<Integer, Integer> starOrdersForCourtPosition;
	static {
		starOrdersForCourtPosition = new HashMap<Integer, Integer>();
		starOrdersForCourtPosition.put(1, 3);
		starOrdersForCourtPosition.put(2, 3);
		starOrdersForCourtPosition.put(3, 2);
		starOrdersForCourtPosition.put(4, 1);
		starOrdersForCourtPosition.put(5, 0);
		starOrdersForCourtPosition.put(6, 0);
	}
	private House house;
	private Map<String, Order> orderTypesMap = new HashMap<String, Order>();
	private List<Order> availableOrders;
	private List<Order> usedOrders;
	private int availableStarOrders;
	private int usedStarOrders;

	public void assignOrder(Order order, Region region) {
		availableOrders.remove(order);
		usedOrders.add(order);
		region.setOrder(order);
		order.setRegion(region);
		if (order.isStarOrder()) {
			usedStarOrders++;
			updateStarOrdedsCount();
		}
	}

	public void returnOrder(Order order) {
		usedOrders.remove(order);
		availableOrders.add(order);
		if (order.isStarOrder()) {
			usedStarOrders--;
			updateStarOrdedsCount();
		}
	}

	public void resetOrders(int availableStarOrders) {
		for (Order order : usedOrders) {
			order.getRegion().setOrder(null);
			order.setRegion(null);
		}
		this.availableStarOrders = availableStarOrders;
		usedStarOrders = 0;
		availableOrders.addAll(usedOrders);
		updateStarOrdedsCount();
	}

	public void initialize(int availableStarOrders) {
		if (orderTypesMap == null || orderTypesMap.size() == 0) {
			throw new IllegalStateException("Orders map not initialized");
		}
		availableOrders = new LinkedList<Order>();
		usedOrders = new LinkedList<Order>();
		availableOrders.addAll(orderTypesMap.values());
		for(Order order:availableOrders){
			order.setOrdersPool(this);
		}
		resetOrders(availableStarOrders);
	}

	public List<String> getAvailableOrdersString() {
		List<String> strings = new LinkedList<String>();
		for (Order order : availableOrders) {
			strings.add(order.code);
		}
		return strings;
	}

	private void updateStarOrdedsCount() {
		if (usedStarOrders == availableStarOrders) {
			makeUpdate(availableOrders, usedOrders, false);
		} else if (usedStarOrders < availableStarOrders) {
			makeUpdate(usedOrders, availableOrders, true);
		} else {
			throw new IllegalStateException("Someone tries to use more stars than he has!");
		}
	}

	private void makeUpdate(List<Order> toRemove, List<Order> toAdd, boolean checkifAssigned) {
		List<Order> ordersToRemove = new LinkedList<Order>();
		for (Order availOrder : toRemove) {
			if (availOrder.isStarOrder() && (!checkifAssigned || availOrder.getRegion() == null)) {
				ordersToRemove.add(availOrder);
			}
		}
		toRemove.removeAll(ordersToRemove);
		toAdd.addAll(ordersToRemove);
	}

	public List<Order> getUsedRaidOrders(){

		List<Order> executableRaidOrders = new LinkedList<Order>();
		for (Order order : usedOrders) {
			if (order instanceof RaidOrder && order.getRegion() != null) {
				executableRaidOrders.add(order);
			}
		}
		return executableRaidOrders;
	}
	
	

	public List<Order> getAvailableOrders() {
		return availableOrders;
	}

	public void setAvailableOrders(List<Order> availableOrders) {
		this.availableOrders = availableOrders;
	}

	public List<Order> getUsedOrders() {
		return usedOrders;
	}

	public void setUsedOrders(List<Order> usedOrders) {
		this.usedOrders = usedOrders;
	}

	public int getAvailableStarOrders() {
		return availableStarOrders;
	}

	public void setAvailableStarOrders(int availableStarOrders) {
		this.availableStarOrders = availableStarOrders;
	}

	public Map<String, Order> getOrderTypesMap() {
		return orderTypesMap;
	}

	public void setOrderTypesMap(Map<String, Order> orderTypesMap) {
		this.orderTypesMap = orderTypesMap;
	}

	public int getUsedStarOrders() {
		return usedStarOrders;
	}

	public void setUsedStarOrders(int usedStarOrders) {
		this.usedStarOrders = usedStarOrders;
	}

	public House getHouse() {
		return house;
	}

	public void setHouse(House house) {
		this.house = house;
	}

}
