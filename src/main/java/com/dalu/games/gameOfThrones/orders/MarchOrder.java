package com.dalu.games.gameOfThrones.orders;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.dalu.games.gameOfThrones.main.GameplayManager;
import com.dalu.games.gameOfThrones.map.Region;
import com.dalu.games.gameOfThrones.units.Unit;
import com.sun.istack.logging.Logger;

public class MarchOrder extends Order {

	public MarchOrder(int strength, GameplayManager gameplayManager, String code, boolean starOrder) {
		super(strength, gameplayManager, code, starOrder);
	}

	@Override
	public void execute() {
		for (int i = 0; i < region.getUnits().size(); i++) {
			Unit unit = region.getUnits().get(i);
			List<Region> reachableRegions = new LinkedList<Region>();
			for (Entry<String, Region> regionEntry : gameplayManager.getGame().getRegions().entrySet()) {
				if (unit.canMoveTo(regionEntry.getValue())) {
					reachableRegions.add(regionEntry.getValue());
				}
			}

			Region moveTo = gameplayManager.whereToMove(unit, this.region, reachableRegions);
			if (moveTo != null && reachableRegions.contains(moveTo)) {
				if (moveTo != region) {
					unit.getRegion().getUnits().remove(unit);
					unit.setRegion(moveTo);
					moveTo.getUnits().add(unit);
					i--;
				}
			} else {
				Logger.getLogger(MarchOrder.class).warning("No destination region found in region's map");
			}
		}
		ordersPool.returnOrder(this);
	}
}
