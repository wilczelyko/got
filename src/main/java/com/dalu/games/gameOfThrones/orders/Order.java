package com.dalu.games.gameOfThrones.orders;

import com.dalu.games.gameOfThrones.main.GameplayManager;
import com.dalu.games.gameOfThrones.map.Region;

public abstract class Order {
	protected int strength;
	protected Region region;
	protected GameplayManager gameplayManager;
	protected String code;
	protected boolean starOrder;
	protected OrdersPool ordersPool;

	public abstract void execute();

	public Order(int strength, GameplayManager gameplayManager, String code, boolean starOrder) {
		super();
		this.strength = strength;
		this.gameplayManager = gameplayManager;
		this.code = code;
		this.starOrder = starOrder;
	}

	public Order(GameplayManager gameplayManager, String code, boolean starOrder) {
		super();
		this.gameplayManager = gameplayManager;
		this.code = code;
		this.starOrder = starOrder;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isStarOrder() {
		return starOrder;
	}

	public void setStarOrder(boolean starOrder) {
		this.starOrder = starOrder;
	}

	public OrdersPool getOrdersPool() {
		return ordersPool;
	}

	public void setOrdersPool(OrdersPool ordersPool) {
		this.ordersPool = ordersPool;
	}

}
